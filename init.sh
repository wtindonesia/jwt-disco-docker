#run database migration
docker exec ${PWD##*/}_web_1 artisan october:up
#set default theme ke JWT Disco
docker exec ${PWD##*/}_web_1 artisan theme:use jwtdisco
#install rainlab.builder plugin
docker exec ${PWD##*/}_web_1 artisan plugin:install rainlab.builder