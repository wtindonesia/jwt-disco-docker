<?php namespace Mirum\Stars\Components;

use Lang;
use Auth;
use Mail;
use Event;
use Flash;
use Input;
use Request;
use Redirect;
use Validator;
use ValidationException;
use ApplicationException;
use Cms\Classes\Page;
use Cms\Classes\ComponentBase;
use Cms\Classes\Theme;
use RainLab\User\Models\Settings as UserSettings;
use Exception;
use System\Models\File;
use RainLab\User\Components\Account as UserAccountComponent;
use Response;

class Star extends UserAccountComponent
{
    public function componentDetails()
    {
        return [
            'name'        => 'Stars',
            'description' => 'User management form.'
        ];
    }

    /**
     * Sign in the user
     */
    public function onSigninStar()
    {
        try {
            /*
             * Validate input
             */
            $rules = [];
            $data = post();
            $rules['login'] = $this->loginAttribute() == UserSettings::LOGIN_USERNAME
                ? 'required|between:2,255'
                : 'required|email|between:6,255';

            $rules['password'] = 'required|between:4,255';

            if (!array_key_exists('login', $data)) {
                $data['login'] = post('username', post('email'));
            }

            $validation = Validator::make($data, $rules);
            if ($validation->fails()) {
                return ['#result' => $this->renderPartial('@signin_messages.htm', [
                    'errorMsgs' => $validation->messages()->all(),
                    'fieldMsgs' => $validation->messages()
                ])];
            }

            /*
             * Authenticate user
             */
            $credentials = [
                'login'    => array_get($data, 'login'),
                'password' => array_get($data, 'password')
            ];

            Event::fire('rainlab.user.beforeAuthenticate', [$this, $credentials]);

            $user = Auth::authenticate($credentials, true);
            if ($user->isBanned()) {
                Auth::logout();
                throw new AuthException(/*Sorry, this user is currently not activated. Please contact us for further assistance.*/'rainlab.user::lang.account.banned');
            }

            /*
             * Redirect
             */
            if ($redirect = $this->makeRedirection(true)) {
                return $redirect;
            }
        }
        catch (Exception $ex) {
            if (Request::ajax()) throw $ex;
            else Flash::error($ex->getMessage());
        }
    }

    public function onSubmitPayment()
    {   
        $data = Input();
        $rules = [
                'payment_receipt'   => 'required|mimes:pdf,jpg,jpeg,png,svg,bmp|max:10000',
            ];

        $validate = array();
        $validation = Validator::make($data, $rules);
            if ($validation->fails()) {
                return [
                    '#result-submitfile' => $this->renderPartial('@submit_file_messages.htm', 
                                    [
                                    'errorMsgs' => $validation->messages()->all(),
                                    'fieldMsgs' => $validation->messages()
                                ]),
                'success_upload' => false
                ];
            }
        //$data['payment_receipt'] = Input::file('payment_receipt');

        if (!$user = $this->user()) {
            return;
        }
        if (Input::hasFile('payment_receipt')) {
            $user->payment_receipt = Input::file('payment_receipt');
        }
        $save = $user->save();
        if($save){
            $validate = ['success_upload' => true];
        }
        Flash::success(post('flash', "Thank you, your payment is received! Make sure you've completed other steps, and wait for the brief on October 10th, 2018"));
        
        return $validate;
        /*
         * Redirect
         */
        /*if ($redirect = $this->makeRedirection()) {
            return $redirect;
        }*/
        /*$this->prepareVars();*/
    }

    public function onSubmitWork()
    {   
        $data = Input();
        $rules = [
                'submit_work'   => 'required|mimes:pdf|max:10000',
            ];

        $validate = array();
        $validation = Validator::make($data, $rules);
            if ($validation->fails()) {
                return [
                    '#result-submitfile' => $this->renderPartial('@submit_file_messages.htm', 
                                    [
                                    'errorMsgs' => $validation->messages()->all(),
                                    'fieldMsgs' => $validation->messages()
                                ]),
                'success_upload' => false
                ];
            }
        //$data['submit_work'] = Input::file('submit_work');

        if (!$user = $this->user()) {
            return;
        }
        if (Input::hasFile('submit_work')) {
            $user->submit_work = Input::file('submit_work');
        }
        $save = $user->save();
        if($save){
            $validate = ['success_upload' => true];
        }
        Flash::success(post('flash', "Thank you for your submission. The finalists will be announced on October 24th, 2018."));
        return $validate;
        /*
         * Redirect
         */
        /*if ($redirect = $this->makeRedirection()) {
            return $redirect;
        }*/
        $this->prepareVars();
    }

    public function onSubmitCampusLetter()
    {   
        $data = Input();
        $rules = [
                'campus_letter'   => 'required|mimes:pdf|max:10000',
            ];
        $validate = array();
        $validation = Validator::make($data, $rules);
            if ($validation->fails()) {
                return [
                    '#result-submitfile' => $this->renderPartial('@submit_file_messages.htm', 
                                    [
                                    'errorMsgs' => $validation->messages()->all(),
                                    'fieldMsgs' => $validation->messages()
                                ]),
                'success_upload' => false
                ];
            }
        //$data['submit_work'] = Input::file('submit_work');

        if (!$user = $this->user()) {
            return;
        }
        if (Input::hasFile('campus_letter')) {
            $user->campus_letter = Input::file('campus_letter');
        }
         $save = $user->save();
        if($save){
            $validate = ['success_upload' => true];
        }

        Flash::success(post('flash', "Thank you for registering! You have 48 hours to complete the payment. Check the details on rules number 6."));
        return $validate;

        /*
         * Redirect
         */
        /*if ($redirect = $this->makeRedirection()) {
            return $redirect;
        }*/
        $this->prepareVars();
    }

    /**
     * Register the user
     */
    public function onRegisterStar()
    {
        try {
            if (!$this->canRegister()) {
                throw new ApplicationException(Lang::get(/*Registrations are currently disabled.*/'rainlab.user::lang.account.registration_disabled'));
            }

            /*
             * Validate input
             */
            $data = post();

            if (!array_key_exists('password_confirmation', $data)) {
                $data['password_confirmation'] = post('password');
            }

            // if (Input::hasFile('avatar')) {
            $dob = date('Y-m-d', strtotime(str_replace('/','-', $data['dob'])));
            $data['avatar'] = Input::file('avatar');
            //$data['campus_letter'] = Input::file('campus_letter');
            $data['dob'] = $dob;
            // }

            $rules = [
                'name'    => 'required|between:2,255',
                'email'    => 'required|email|unique:users|between:6,255',
                'password' => 'required|between:4,255|confirmed',
                'name'    => 'required|between:2,255',
                'dob'    => 'required|date',
                'university' => 'required',
                'mobile' => 'required|numeric',
                'avatar'   => 'required|image|max:4000',
                //'campus_letter'   => 'required|mimes:pdf|max:10000'
            ];

            if ($this->loginAttribute() == UserSettings::LOGIN_USERNAME) {
                $rules['username'] = 'required|between:2,255';
            }

            $validation = Validator::make($data, $rules);
            if ($validation->fails()) {
                return ['#result-register' => $this->renderPartial('@register_messages.htm', [
                    'errorMsgs' => $validation->messages()->all(),
                    'fieldMsgs' => $validation->messages()
                ])];
            }

            /*
             * Register user
             */
            Event::fire('rainlab.user.beforeRegister', [&$data]);

            $requireActivation = UserSettings::get('require_activation', true);
            $automaticActivation = UserSettings::get('activate_mode') == UserSettings::ACTIVATE_AUTO;
            $userActivation = UserSettings::get('activate_mode') == UserSettings::ACTIVATE_USER;
            $user = Auth::register($data, $automaticActivation);

            if (Input::hasFile('avatar')) {
                $user->avatar = Input::file('avatar');
            }
            /*if (Input::hasFile('campus_letter')) {
                $user->campus_letter = Input::file('campus_letter');
            }*/
            Event::fire('rainlab.user.register', [$user, $data]);
            /*
             * Activation is by the user, send the email
             */
            if ($userActivation) {
                $this->sendActivationEmail($user);

                Flash::success(Lang::get(/*An activation email has been sent to your email address.*/'rainlab.user::lang.account.activation_email_sent'));
            }else{
                Flash::success('You are successfully registered.');
            }

            /*
             * Automatically activated or not required, log the user in
             */
            if ($automaticActivation || !$requireActivation) {
                Auth::login($user);
            }

            /*
             * Redirect to the intended page after successful sign in
             */
            if ($redirect = $this->makeRedirection(true)) {
                return $redirect;
            }
        }
        catch (Exception $ex) {
            if (Request::ajax()) throw $ex;
            else Flash::error($ex->getMessage());
        }
    }

    /**
     * Log out the user
     *
     * Usage:
     *   <a data-request="onLogout">Sign out</a>
     *
     * With the optional redirect parameter:
     *   <a data-request="onLogout" data-request-data="redirect: '/good-bye'">Sign out</a>
     *
     */
    public function onLogoutStar()
    {
        $user = Auth::getUser();

        Auth::logout();

        if ($user) {
            Event::fire('rainlab.user.logout', [$user]);
        }

        $url = post('redirect', Request::fullUrl());

        //Flash::success(Lang::get('rainlab.user::lang.session.logout'));

        return Redirect::to($url);
    }

    public function onImageUpload(){
        $data = Input::all();

        $file = (new File())->fromPost($data['avatar']);

        return[
            '#imageResult' => '<img src="' . $file->getThumb(200,200, ['mode' => 'crop']) . '">'
        ];
    }

    function onDownloadButtonClicked(){
        $pathToFile = Theme::getActiveTheme()->getPath() . '/assets/pdfs/Wabisabi_The_Art_of_Imperfection.pdf';
        $fileName = "Wabisabi The Art of Imperfection.pdf";
        $headers = [
            'HTTP/1.1 200 OK',
            'Pragma: public',
            'Content-Type: application/pdf'
        ];
        return Response::download($pathToFile, $fileName, $headers);
    } 

    function onDownloadButtonClickedBrief(){
        $pathToFile = Theme::getActiveTheme()->getPath() . '/assets/pdfs/JWT_DISCO_2018_-_QUALIFICATION ROUND_BRIEF.pdf';
        $fileName = "JWT DISCO 2018 - QUALIFICATION ROUND BRIEF.pdf";
        $headers = [
            'HTTP/1.1 200 OK',
            'Pragma: public',
            'Content-Type: application/pdf'
        ];
        return Response::download($pathToFile, $fileName, $headers);
    }

    //
    // Properties
    //

    /**
     * Returns the logged in user, if available
     */
    public function user()
    {
        if (!Auth::check()) {
            return null;
        }

        return Auth::getUser();
    }

    /**
     * Redirect to the intended page after successful update, sign in or registration.
     * The URL can come from the "redirect" property or the "redirect" postback value.
     * @return mixed
     */
    protected function makeRedirection($intended = false)
    {
        $method = $intended ? 'intended' : 'to';

        $property = $this->property('redirect');

        if (strlen($property) && !$property) {
            return;
        }

        $redirectUrl = $this->pageUrl($property) ?: $property;

        if ($redirectUrl = post('redirect', $redirectUrl)) {
            return Redirect::$method($redirectUrl);
        }
    }

}