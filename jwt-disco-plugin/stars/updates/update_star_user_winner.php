<?php namespace Mirum\Stars\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class UpdateStarUserWinner extends Migration
{

    public function up()
    {
        Schema::table('users', function($table)
        {
            $table->boolean('is_winner')->default(false);
        });
    }

    public function down()
    {
        Schema::table('users', function($table)
        {
            $table->dropColumn([
                'is_winner'
            ]);
        });
    }

}