<?php namespace Mirum\Stars\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class UpdateStarUserDropJobTitle extends Migration
{

    public function up()
    {
        Schema::table('users', function($table)
        {
            $table->dropColumn('job_title');
        });
    }

    public function down()
    {
        Schema::table('users', function($table)
        {
            $table->string('job_title')->after('password')->default('null');
        });
    }

}