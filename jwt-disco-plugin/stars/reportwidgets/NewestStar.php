<?php namespace Mirum\Stars\ReportWidgets;

use Backend\Classes\ReportWidgetBase;
use Rainlab\User\models\User as UserModel;

class NewestStar extends ReportWidgetBase
{

    public function render()
    {
        $this->vars['newestTen'] = UserModel::newestTen()->get();

        return $this->makePartial('widget');
    }
}