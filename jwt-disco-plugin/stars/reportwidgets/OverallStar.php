<?php namespace Mirum\Stars\ReportWidgets;

use Backend\Classes\ReportWidgetBase;
use Rainlab\User\models\User as UserModel;
use Carbon\Carbon;

class OverallStar extends ReportWidgetBase
{

    public function render()
    {
        $this->vars['countActive'] = UserModel::isActivated()->count();
        $this->vars['countInnactive'] = UserModel::IsInnactivated()->count();

        $this->vars['countStar'] = UserModel::count();
        
        $now = Carbon::now();
        $start = $now->startOfWeek();
        $end = $now->endOfWeek();
        $thisWeekRegister = UserModel::whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->count();
        $prevWeekRegister = UserModel::whereBetween('created_at', [Carbon::now()->subDays(7)->startOfWeek(), Carbon::now()->subDays(7)->endOfWeek()])->count();


        $this->vars['prevWeekRegister'] = $prevWeekRegister;
        $this->vars['diffWeekRegister'] = $thisWeekRegister - $prevWeekRegister;

        return $this->makePartial('widget');
    }
}