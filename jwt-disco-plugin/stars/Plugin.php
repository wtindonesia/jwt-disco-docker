<?php namespace Mirum\Stars;

use System\Classes\PluginBase;
use Rainlab\User\models\User as UserModel;
use Rainlab\User\Controllers\Users as UsersController;
use Rainlab\User\Plugin as UsersPlugin;
use Event;
use Backend;

class Plugin extends PluginBase
{
    public $require = ['RainLab.User'];

    public function pluginDetails()
    {
        return [
            'name'        => 'Stars',
            'description' => 'Provides extended features used from user plugin.',
            'author'      => 'Mirum',
            'icon'        => 'icon-star'
        ];
    }

    public function registerComponents()
    {
        return [
            'Mirum\Stars\Components\Star' => 'Star'
        ];
    }

    public function registerReportWidgets()
    {
        return [
            'Mirum\Stars\ReportWidgets\OverallStar' => [
                'label'   => 'Get Overall Data of Star',
                'context' => 'dashboard'
            ],
            'Mirum\Stars\ReportWidgets\NewestStar' => [
                'label'   => 'Get Ten Newest Star',
                'context' => 'dashboard'
            ]
        ];
    }

    public function registerSettings()
    {
    }

    public function boot()
    {
        // add fillable, relation, and scoper to user model
        UserModel::extend(function($model) {
            $model->addFillable([
                'dob',
                'university',
                'mobile',
            ]);

            $model->attachOne = [
                'avatar' => \System\Models\File::class,
                'campus_letter' => \System\Models\File::class,
                'payment_receipt' => \System\Models\File::class,
                'submit_work' => \System\Models\File::class
            ];

            $model->addDynamicMethod('scopeNewestTen', function($query) {
                return $query->orderBy('created_at', 'desc')->take(10);
            });
            $model->addDynamicMethod('scopeIsInnactivated', function($query) {
                return $query->where('is_activated', 0);
            });
        });

        // add sidemenu prevstars to user menu
        Event::listen('backend.menu.extendItems', function($manager) {
            $manager->addSideMenuItems('RainLab.User', 'user', [
                'prevstars' => [
                    'label' => 'Prevstars', 
                    'icon'  => 'icon-star-half-o',
                    'code'  => 'prevstars',
                    'owner' => 'Mirum.Prevstars',
                    'url'   => Backend::url('mirum/prevstars/prevstars')
                ]
            ]);
    
        });

        //add custom backend to front
         Event::listen('backend.list.extendColumns', function($widget) {
             // Only for the User controller
            if (!$widget->getController() instanceof \RainLab\User\Controllers\Users) {
                return;
            }

            // Only for the User model
            if (!$widget->model instanceof \RainLab\User\Models\User) {
                return;
            }

            // Add an extra campus letter column
            $widget->addColumns([
                'campus_letter' => [
                    'label' => 'Campus letter file',
                    'type' => 'switch',
                    'sortable' => false,
                ],
                'payment_receipt' => [
                    'label' => 'Payment Receipt  file',
                    'type' => 'switch',
                    'sortable' => false,
                ],
                'submit_work' => [
                    'label' => 'Submit Work  file',
                    'type' => 'switch',
                    'sortable' => false,
                ],
            ]);

            // Remove a Surname column
            $widget->removeColumn('surname');
        });
        
        // add custom form to backend user form
        UsersController::extendFormFields(function($form, $model, $context){
            $form->addTabFields([
                'is_winner' => [
                    'label' => 'Disco Winner',
                    'type' => 'switch',
                    'span' => 'auto',
                    'tab' => 'Profile',
                ],
                'dob' => [
                    'label' => 'Date of Birth',
                    'type' => 'datepicker',
                    'mode' => 'date',
                    'default' => 'now',
                    'format' => 'Y M d',
                    'minDate' => '1945-01-01',
                    'maxDate' => 'now',
                    'span' => 'auto',
                    'tab' => 'Profile'
                ],
                'mobile' => [
                    'label' => 'Mobile Phone',
                    'type' => 'text',
                    'span' => 'auto',
                    'tab' => 'Profile',
                ],
                'university' => [
                    'label' => 'University',
                    'type' => 'text',
                    'span' => 'auto',
                    'tab' => 'Profile'
                ],
                'is_payment_confirmation' => [
                    'label' => 'Payment Confirmation',
                    'type' => 'switch',
                    'span' => 'auto',
                    'tab' => 'File',
                ],
                'campus_letter' => [
                    'label' => 'Campus Letter',
                    'type' => 'fileupload',
                    'mode' => 'file',
                    'tab' => 'File'
                ],
                'payment_receipt' => [
                    'label' => 'Payment Receipt',
                    'type' => 'fileupload',
                    'mode' => 'file',
                    'tab' => 'File'
                ],
                'submit_work' => [
                    'label' => 'Submit Work',
                    'type' => 'fileupload',
                    'mode' => 'file',
                    'tab' => 'File'
                ],
                
            ]);
        });
    }
}
