<?php namespace Mirum\Rules\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateMirumRules extends Migration
{
    public function up()
    {
        Schema::create('mirum_rules_', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('title');
            $table->string('slug');
            $table->text('description');
            $table->integer('order');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('mirum_rules_');
    }
}