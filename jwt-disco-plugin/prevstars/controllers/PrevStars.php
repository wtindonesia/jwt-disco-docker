<?php namespace Mirum\Prevstars\Controllers;

use Backend\Classes\Controller;
use BackendMenu;
use Lang;
use Flash;
use Mirum\Prevstars\Models\prevstar;

class PrevStars extends Controller
{
    public $implement = [        'Backend\Behaviors\ListController',        'Backend\Behaviors\FormController'    ];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Mirum.Prevstars', 'main-menu-item');
    }

    public function onCheckIsFinalist($id = null){
        
        $checkedIds = post('checked');

        if (!$checkedIds || !is_array($checkedIds) || !count($checkedIds)) {
            Flash::error('There are no selected records to check.');
            return $this->listRefresh();
        }

        foreach($checkedIds as $checkedId) {
            $starSelected = Prevstar::find($checkedId);
            $starSelected->is_finalist = true;
            $starSelected->save();
            trace_log($starSelected->name . ' - ' . $starSelected->is_finalist);
        }
        Flash::success('Checked selected records.');

        return $this->listRefresh();
    }
}
