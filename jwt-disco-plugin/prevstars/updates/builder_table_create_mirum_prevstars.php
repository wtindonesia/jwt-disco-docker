<?php namespace Mirum\Prevstars\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateMirumPrevstars extends Migration
{
    public function up()
    {
        Schema::create('mirum_prevstars_', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('name');
            $table->string('job_title');
            $table->boolean('is_finalist');
            $table->boolean('is_winner');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('mirum_prevstars_');
    }
}