$(document).ready(function(){

  $('#fullpage').fullpage({
    licenseKey: 'OPEN-SOURCE-GPLV3-LICENSE',
    anchors: ['#home', '#about', '#rules', '#judges', '#discostar', '#pastwinners', '#profile'],
    scrollHorizontally: true,
    menu: ".navbar-nav",
    normalScrollElements: '.modal'
  });

  $(".smooth-scroll").on('click', function(event) {
    var id = $(this).attr('href');
    fullpage_api.moveTo(id);
    $(this).closest('.navbar-collapse').removeClass('show');
  });

  function addAutoHeight() {
    $('.section').each(function(i, obj) {
      $(obj).addClass('fp-auto-height');
    });
  }

  function removeAutoHeight() {
    $('.section').each(function(i, obj) {
      $(obj).removeClass('fp-auto-height');
    });
  }

  if ($(window).width() < 768 || $(window).height() < 600) {
    fullpage_api.setResponsive(true);
    // addAutoHeight();
    $("#fullpage").css('overflow-x', 'hidden');
  } else {
    fullpage_api.setResponsive(false);
    removeAutoHeight();
    $("#fullpage").css('overflow-x', 'visible');
  }

  $(window).on('resize', function(){
    var win = $(this);
    if (win.width() < 768 || win.height() < 600) {
      fullpage_api.setResponsive(true);
      // addAutoHeight();
      $("#fullpage").css('overflow-x', 'hidden');
    } else {
      fullpage_api.setResponsive(false);
      removeAutoHeight();
      $("#fullpage").css('overflow-x', 'visible');
    }
  });

  // var vid = document.getElementById("myVideo");

  // function playVid() {
  //     vid.play();
  // }

  // function pauseVid() {
  //     vid.pause();
  // }

  // $('.js-tilt').tilt({
  //   maxTilt: 5
  // });

  $("#registerLink").on('click', function(event) {
    $("#loginModal .login").hide();
    $("#loginModal .register").fadeIn(500);
  });

  $("#loginLink").on('click', function(event) {
    $("#loginModal .register").hide();
    $("#loginModal .login").fadeIn(500);
  });

  $(".datepicker").datepicker({
    format: "dd/mm/yyyy",
    endDate: "today",
    autoclose: true,
    todayHighlight: true
  });

  $("#inputReference").change(function(e){
    var fileName;
    if ($(this).val() != '') {
      fileName = e.target.files[0].name;
    } else {
      fileName = 'No File Selected';
    }
    $("#campus_letter").val(fileName);
  });

  $("#inputPayment").change(function(e){
    var fileName;
    if ($(this).val() != '') {
      fileName = e.target.files[0].name;
    } else {
      fileName = 'No File Selected';
    }
    $("#payment_receipt").val(fileName);
  });

  $("#inputWork").change(function(e){
    var fileName;
    if ($(this).val() != '') {
      fileName = e.target.files[0].name;
    } else {
      fileName = 'No File Selected';
    }
    $("#submit_work").val(fileName);
  });

  $(".modal-trigger").on('click', function(event) {
    var data = $(this).find('.modal-data').html();
    $("#popupModal .modal-body").empty();
    $("#popupModal .modal-body").html(data);
  });

  //stop vid

  $('body').on('hidden.bs.modal', '.modal', function () {
    $('video').trigger('pause');
  });

  // $(document).ready(function() {
  //   $('.smooth-scroll').bind('click', function(e) {
  //     e.preventDefault(); // prevent hard jump, the default behavior

  //     var target = $(this).attr("href"); // Set the target as variable

  //     // perform animated scrolling by getting top-position of target-element and set it as scroll target
  //     $('html, body').stop().animate({
  //         scrollTop: $(target).offset().top
  //     }, 600, function() {
  //         location.hash = target; //attach the hash (#jumptarget) to the pageurl
  //     });

  //     return false;
  //   });
  // });

  $(window).scroll(function() {
    var scrollDistance = $(window).scrollTop();

    // Assign active class to nav links while scolling
    $('.section').each(function(i) {
      if ($(this).position().top <= scrollDistance) {
        $('.navbar-nav .nav-item.active').removeClass('active');
        $('.navbar-nav .nav-item').eq(i).addClass('active');
      }
    });
  }).scroll();

});
