'use strict';

var config = {
  nodePath: 'node_modules/',
  assetsPath: './assets/',
}

var gulp = require('gulp'),
    sass = require('gulp-sass'),
    sourceMaps = require('gulp-sourcemaps'),
    minify = require('gulp-minify'),
    rename = require('gulp-rename'),
    cleanCss = require('gulp-clean-css'),
    stripCssCommments = require('gulp-strip-css-comments');

var sassOptions = {
  errLogToConsole: true,
  outputStyle: 'expanded',
  // includePaths: [config.nodePath + 'bootstrap/scss/']
}

/*
 * sass / scss compile
 */
gulp.task('sass', function () {
  return gulp.src('./assets/scss/*.scss')
    .pipe( sourceMaps.init() )
    .pipe( sass( sassOptions ).on('error', sass.logError))
    .pipe( gulp.dest( './assets/css' ) )
    .pipe( cleanCss() )
    .pipe( rename( { suffix: '.min' }))
    .pipe( stripCssCommments({ preserve: /^# sourceMappingURL=/}) )
    .pipe( sourceMaps.write('.') )
    .pipe( gulp.dest( './assets/css' ) );
});

/*
 * js
 */
gulp.task('js', function() {
  return gulp.src('./assets/js/main.js' )
    .pipe( gulp.dest('./assets/js' ))
    .pipe( minify({
      ext: {
        min: '.js',
      },
      noSource: true
    }))
    .pipe( rename( { suffix: '.min' } ))
    .pipe( gulp.dest('./assets/js' ));
})

/*
 * watch: js & scss
 */
gulp.task('watch', function () {
   gulp.watch( config.assetsPath + 'js/**/*.js', ['js'] );
   gulp.watch( config.assetsPath + 'scss/**/*.scss', ['sass']);
})

/*
 * default task
 */
gulp.task('default', ['watch']);